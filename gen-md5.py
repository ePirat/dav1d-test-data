#!/usr/bin/env python3
import os, argparse, subprocess
from pathlib import Path

parser = argparse.ArgumentParser()

# Input file
parser.add_argument("files", metavar='FILE', nargs='+', help='input file')

# Test line output file
parser.add_argument("--mesonfile",
    type=argparse.FileType('a', encoding='UTF-8'),
    help='meson build file to which to append test calls')

args = parser.parse_args()

for filepath in args.files:
    # Call aomdec to obtain the md5
    res = subprocess.run([
        "aomdec",
        "--output-bit-depth=8", "--md5", "--rawvideo",
        filepath
        ],
        stdout=subprocess.PIPE, check=True, encoding='UTF-8')

    md5, _ = res.stdout.split(' ', 1)

    md5filepath = filepath + '.md5'
    print('Writing md5 "' + md5 + '" to ' + md5filepath)
    with open(md5filepath, mode='x', encoding='UTF-8') as outfile:
        outfile.write(md5 + '\n')

    if args.mesonfile:
        test_name = Path(filepath).stem
        args.mesonfile.write(
                (
                "test('Test {name}', md5test,\n"
                "    args: [dav1d, files('{filepath}')])\n"
                ).format(name=test_name, filepath=filepath)
            )

if args.mesonfile:
    args.mesonfile.close()
