#!/usr/bin/env python3
import os, argparse, subprocess

parser = argparse.ArgumentParser()

# Input file
parser.add_argument("dav1d", metavar='EXE', help='path to the dav1d executable test with')
parser.add_argument("file", metavar='FILE', help='input file to test')
args = parser.parse_args()

md5filepath = args.file + '.md5'

# Read md5 to compare against
with open(md5filepath, mode='r', encoding='UTF-8') as md5file:
    md5_comp = md5file.readline().strip()

# Call dav1d to obtain the md5
res = subprocess.run([
    args.dav1d,
    "-q", "--muxer", "md5", "-o", "-",
    "-i", args.file
    ],
    stdout=subprocess.PIPE, check=True, encoding='UTF-8')

md5 = res.stdout.strip()

print('Comparing dav1d md5 "' + md5 + '" with "' + md5_comp + '"')

if (md5 == md5_comp):
    exit(0)

exit(1)
